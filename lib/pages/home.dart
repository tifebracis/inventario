
import 'package:flutter/material.dart';
import '../models/user.dart';
import '../helpers/databaseHelper.dart';
import 'listProdutos.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../helpers/handlerUser.dart';
import 'createInvent.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var formKey = new GlobalKey<FormState>();

  Set<String> listErros = new Set<String>();
  String stateDevice = '';

  bool loading = false;

  User user = new User();

  DatabaseHelper db = new DatabaseHelper();

  void initState() {
    super.initState();
    checkConect();
  }

  void checkConect() async{
    await checkConection().then((stateDev){
      print(stateDev);
      setState(() {
        stateDevice = stateDev;
      });
      if(stateDev == 'offline'){
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => CreateInvent(userId: '')),
        );
      }
    });
  }

  void stateLoading(){
    setState(() {
      loading = !loading;
    });
  }

  void loginFunc() async {
    await checkConect();
    stateLoading();
    if(validatedFields() && stateDevice == 'online'){
      print(user.toMap());
      try{
        FirebaseUser userId = (await FirebaseAuth.instance.signInWithEmailAndPassword(email: user.email.trim(),password: user.password.trim())) as FirebaseUser;
        if(userId != null){
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => CreateInvent(userId: userId.uid)),
          );
        }
      }catch(e){
        showDialog(context:context,
          builder: (BuildContext context) { 
            return (
              AlertDialog(
                title: Text(e.toString().toLowerCase().contains('network') ? "Verifique sua conexão com a internet." : "Email ou senha inválidos."),
                backgroundColor: Color.fromRGBO(160, 34, 37, 1),
                titleTextStyle: TextStyle(
                  color: Colors.white,
                  fontSize: 20
                ),
              )
            );
          }
        );
      }
    }
    stateLoading();
  }

  bool validatedFields(){
    var form = formKey.currentState;
    if(form.validate())
      return true;
    return false;
  }

  void input(String label, bool obscure){
    String initvalue = label.contains('Email') ? user.email : label.contains('Confirmar') ? user.confirmPassword : label.contains('Senha') ? user.password : '';
    showDialog(
      context:context,
      builder: (BuildContext context) { 
        return (
          AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20)
            ),
            contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            content: TextFormField(
              onFieldSubmitted: (value) {
                Navigator.pop(context, true);
              },
              autofocus: true,
              initialValue: initvalue,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20)
                ),
                labelText: label,
              ),
              obscureText: obscure,
              onChanged: (value){
                setState(() {
                  if(label.contains('Email'))
                    user.email = value;
                  if(label.contains('Senha'))
                    user.password = value;
                  if(label.contains('Confirmar'))
                    user.confirmPassword = value;
                });
              },
            ),
          )
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: stateDevice == 'online' ? Color.fromRGBO(160, 34, 37, 1) : Color.fromRGBO(50, 50, 50, 1),
      body: Center(
        child: Builder(
          builder: (BuildContext context) {
            if(loading){
              return (
                Image(
                  width: 30,
                  image: AssetImage('images/spinner.gif')
                )
              );
            }
            return (
              SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Flex(
                  direction: Axis.vertical,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image(
                      height: 120,
                      image: AssetImage('images/iconFebracis.png'),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)
                      ),
                      margin: EdgeInsets.fromLTRB(15,15,15,5),
                      padding: EdgeInsets.fromLTRB(12,12,12,12),
                      child: Form(
                        key: formKey,
                        child: Flex(
                          direction: Axis.vertical,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Container(  
                              child: TextFormField(
                                initialValue: user.email,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(20)
                                  ),
                                  labelText: 'Email: ',
                                ),
                                enabled: stateDevice == 'online',
                                validator: (value) {
                                  if(value.isEmpty)
                                    return 'Campo obrigatório.';
                                  return null;
                                },
                                onChanged: (value){
                                  setState(() {
                                    user.email = value;
                                  });
                                },
                              )
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                              child: TextFormField(
                                initialValue: user.password,
                                obscureText: true,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(20)
                                  ),
                                  labelText: 'Senha: ',
                                ),
                                enabled: stateDevice == 'online',
                                validator: (value) {
                                  if(value.isEmpty)
                                    return 'Campo obrigatório.';
                                  return null;
                                },
                                onChanged: (value) {
                                  setState(() {
                                    user.password = value;
                                  });
                                },
                              )
                            )
                          ],
                        )
                      )
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
                      child: Row(
                        children: <Widget>[
                          Flexible(
                            flex: 1,
                            child: Container(
                              margin: EdgeInsets.fromLTRB(3, 0, 0, 0),
                              width: double.infinity,
                              child: RaisedButton(
                                color: stateDevice == 'online' ? Colors.black : Colors.white,
                                onPressed: () {
                                  loginFunc();
                                },
                                child: Container(
                                  child: Text(
                                    stateDevice == 'online' ? 'Logar' : 'Entrar Offline',
                                    style: TextStyle(color: stateDevice == 'online' ? Colors.white : Colors.black),
                                  ), 
                                ),
                              ),
                            )
                          )
                        ],
                      ),
                    )
                  ],
                )
              )
            );
          }
        )
      )
    );
  }
}