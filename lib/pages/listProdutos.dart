import 'dart:async';
import 'dart:convert';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';

import '../helpers/handlerUser.dart';
import '../helpers/databaseHelper.dart';

import '../models/product.dart';

import '../components/card.dart';

import '../pages/createInvent.dart';

GlobalKey<_ListProdutosState> globalListProdutosState = new GlobalKey<_ListProdutosState>();

class HandlerListProducts extends StatelessWidget{
  final String userId;
  final String dateInventario;
  final String typeInventario;

  const HandlerListProducts({this.userId,this.dateInventario,this.typeInventario});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return (ListProdutos(key: globalListProdutosState,userId: userId,dateInventario: dateInventario,typeInventario: typeInventario,)
    );
  }

}

class ListProdutos extends StatefulWidget {
  final String userId;
  final String dateInventario;
  final String typeInventario;

  ListProdutos({Key key,this.userId,this.dateInventario,this.typeInventario}) : super(key: key);

  @override
  _ListProdutosState createState() => _ListProdutosState();
}

class _ListProdutosState extends State<ListProdutos> {
  List<Product> products = new List<Product>();
  String stateDevice = '';

  bool loadingUpsertProd = false;
  bool loadingListProd = false;

  ScrollController _scrollController = new ScrollController();

  final fbRef = FirebaseDatabase.instance.reference();

  int quantProducts = 1;

  DatabaseHelper db = new DatabaseHelper();

  void initState() {
    super.initState();
    checkConect();
  }

  void checkConect() async{
    await checkConection().then((stateDev){
      setState(() {
          stateDevice = stateDev;
      });
      getProducts();
    });
  }

  void importDataForCloud(){
    print('oi');
    db.getProducts().then((list) async {
      List<Product> listUpdate = new List<Product>();
      list.forEach((produto){
        produto.userId = widget.userId;
      });
      await Firestore.instance.collection('inventario').getDocuments().then((data){
        data.documents.forEach((product){
          int indexList = list.indexWhere((x) => x.name == product['name']);
          if(indexList >= 0){
            list[indexList].documentId = product.documentID;
            listUpdate.add(list[indexList]);
            list.remove(list[indexList]);
          }
        });
      });
      list.forEach((produto) async{
        await Firestore.instance.collection('inventario').document().setData(produto.toMap());
      });
      listUpdate.forEach((produto) async{
        await Firestore.instance.collection('inventario').document(produto.documentId).updateData(produto.toMap());
      });
    }).then((res){
      db.deleteAllProduct().then((r){
        getProducts();
      });
    });
  }

  void getProducts() async {
    setState(() {
      loadingListProd = true;
    });
    if(stateDevice == 'offline'){
      db.getProducts().then((list){
        setState(() {
          products = list.reversed.toList();
          loadingListProd = false;
        });
      });
    }else{
      fbRef.child('inventario').child(widget.typeInventario).once().then((data){
        List<Product> listProd = new List<Product>();
        var list = [];
        if(data.value != null){
          for(int i = 0; i < data.value.length;i++){
            if(data.value[i]['dateStart'] == widget.dateInventario){
              list = data.value[i]['prods'];
            }
          }
        }
        if(list != null){
          list.forEach((prod){
            if(prod['userId'] == widget.userId)
              listProd.add(Product.fromMap({'name': prod['name'],'quant': prod['quant'],'id': 0,'userId': prod['userId']}));
          });
          print(list);
        }
        setState(() {
          products = listProd.reversed.toList();
          loadingListProd = false;
        });
      });
    }
  }

  void insertProduct(String codBar) async {
    if(stateDevice == 'offline'){
      int id;
      await db.getProducts().then((list){
        list.forEach((product){
          if(product.name == codBar){
            id = product.id;
            setState(() {
              quantProducts = product.quant + quantProducts;
            });
          }
        });
      });
      if(id == null)
        await db.insertProduct(Product(name: codBar, quant: quantProducts,userId: 'userId'));
      else
        await db.updateProduct(Product(id: id,name: codBar, quant: quantProducts,userId: 'userId'));
    }else{
      await fbRef.child('inventario').child(widget.typeInventario).once().then((data){
        var listData;
        var list = [];
        var index;
        var update = false;
        print('oi');
        if(data.value != null){
          for(int i = 0; i < data.value.length; i++){
            print(data.value[i]['dateStart']);
            print(widget.dateInventario);
            print(data.value[i]['dateStart'] == widget.dateInventario);
            print(data.value[i]['prods']);
            if(data.value[i]['dateStart'] == widget.dateInventario){
              listData = data.value;
              index = i;
            }
          }
        }
        if(listData != null){
          if(listData[index]['prods'] != null){
            for(int i = 0; i < listData[index]['prods'].length;i++){
              if(listData[index]['prods'][i]['name'] == codBar){
                update = true;
                if(listData[index]['prods'][i]['userId'] == widget.userId){
                  listData[index]['prods'][i]['quant'] = listData[index]['prods'][i]['quant'] + quantProducts;
                }else{
                  listData[index]['prods'][i]['quant'] = quantProducts;
                  listData[index]['prods'][i]['userId'] = widget.userId; 
                }
                listData[index]['prods'][i]['bat'] = listData[index]['prods'][i]['bat'] + 1;
              }
            }
            list = listData[index]['prods'];
          }else{
            list = [];
          }

          

          if(!update){
            fbRef.child('inventario').child(widget.typeInventario).child(index.toString()).child('prods').update({list.length.toString(): {
              'name': codBar,
              'quant': quantProducts,
              'userId': widget.userId,
              'bat': 1
            }});
          }else{
            fbRef.child('inventario').child(widget.typeInventario).child(index.toString()).child('prods').set(listData[index]['prods']);
          }
        }
      });
    }
    setState(() {
      loadingUpsertProd = false;
    });
  }

  Future clickScanner() async {
    checkConect();
    try{
      String codBar = await BarcodeScanner.scan();
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              'Produto: '+codBar,
              style: TextStyle(color: stateDevice == 'offline' ? Color.fromRGBO(50, 50, 50, 1) : Color.fromRGBO(160, 34, 37, 1)),  
            ),
            titlePadding: EdgeInsets.fromLTRB(20, 20, 20, 0),
            contentPadding: EdgeInsets.fromLTRB(20, 0, 20, 5),
            content: Builder(
              builder: (build){
                if(loadingUpsertProd){
                  return (
                    Text('Inserindo...')
                  );
                }else{
                  return (
                    TextFormField(
                      decoration: InputDecoration(
                        labelText: 'Quantidade: '
                      ),
                      style: TextStyle(color: stateDevice == 'offline' ? Colors.black54 : Color.fromRGBO(160, 34, 37, 1)),
                      keyboardType: TextInputType.numberWithOptions(decimal: false),
                      inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                      initialValue: quantProducts.toString(),
                      onChanged: (value){
                        setState(() {
                          quantProducts = int.parse(value);
                        });
                      },
                    )
                  );
                }
              },
            ),
            actions: <Widget>[
              InkWell(
                onTap: (){
                  if(!loadingUpsertProd)
                    Navigator.pop(context, true);
                },
                child: Container(
                  padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                  child: Text(
                    'Cancel',
                    style: TextStyle(color: stateDevice == 'offline' ? Colors.black54 : Color.fromRGBO(160, 34, 37, 1),fontSize: 15),
                  ),
                ),
              ),
              InkWell(
                onTap: () async{
                  if(!loadingUpsertProd){
                    setState(() {
                      loadingUpsertProd = true;
                    });
                    await insertProduct(codBar);
                    getProducts();
                    Navigator.pop(context, true);
                    setState(() {
                      quantProducts = 1;
                    });
                    Future.delayed(const Duration(milliseconds: 1000), () => clickScanner());
                  }
                },
                child: Container(
                  color: stateDevice == 'offline' ? Color.fromRGBO(50, 50, 50, 1) : Color.fromRGBO(160, 34, 37, 1),
                  padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                  child: Text(
                    'Confirmar',
                    style: TextStyle(color: Colors.white,fontSize: 15),
                  ),
                ),
              )
            ]
          );
        }
      );
      //Future.delayed(Duration(milliseconds: 2000), clickScanner);
    }on PlatformException catch(e){
      print(e);
    }on FormatException catch(e){
      setState(() {
        quantProducts = 1;
      });
    }
  }

  void clickLock(){
    fbRef.child('inventario').child(widget.typeInventario).once().then((data){
      var index;
      if(data.value != null){
        for(var i = 0; i < data.value.length;i++){
          if(data.value[i]['dateStart'] == widget.dateInventario)
            index = i;
        }
      }

      final todayDate = (new DateTime.now().subtract(new Duration(hours: 3)));
      final day = (todayDate.day).toString();
      final month = (todayDate.month).toString();
      final year = (todayDate.year).toString();

      if(index != null){
        fbRef.child('inventario').child(widget.typeInventario).child(index.toString()).update({
          'closed': true,
          'dateClosed': (day.length == 1 ? '0'+day : day)+'/'+(month.length == 1 ? '0'+month : month)+'/'+year
        });
        //globalCreateInventState.currentState.existsInventarioOpen();
        Navigator.pop(context, 'request');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.typeInventario+": "+widget.dateInventario),
        backgroundColor: stateDevice == 'offline' ? Colors.black54 : Color.fromRGBO(160, 34, 37, 1),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {
              checkConect();
              getProducts();
            },
          )
        ],
      ),
      body: Center(
        child: Builder(
          builder: (build){
            if(loadingListProd){
              return (
                Center(  
                  child: Image(
                    width: 30,
                    image: stateDevice == 'offline' ? AssetImage('images/spinner3.gif') : AssetImage('images/spinner2.gif')
                  )
                )
              );
            }else if(products.length > 0){
              return(
                Container(
                  child: NotificationListener(
                    child: ListView.builder(
                      controller: _scrollController,
                      itemCount: products.length,
                      itemBuilder: (context,index){
                        Product product = products[index];
                        return CardItem(prod: product,typeInventario: widget.typeInventario,dateInventario: widget.dateInventario);
                      },
                    ),
                    onNotification: (t) {
                      if (t is ScrollEndNotification) {
                        print(_scrollController.position.pixels);
                      }
                    },
                  )
                )
              );
            }else{
              return(
                Text('')
              );
            }
          },
        ),
      ),
      floatingActionButton: Flex(
        direction: Axis.horizontal,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Container(
            margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
            child: FloatingActionButton(
              heroTag: 'lockButton',
              onPressed: clickLock,
              child: Icon(Icons.lock),
              backgroundColor: stateDevice == 'offline' ? Color.fromRGBO(50, 50, 50, 1) : Color.fromRGBO(160, 34, 37, 1),
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
            child: FloatingActionButton(
              heroTag: 'camButton',
              onPressed: clickScanner,
              child: Icon(Icons.camera_alt),
              backgroundColor: stateDevice == 'offline' ? Color.fromRGBO(50, 50, 50, 1) : Color.fromRGBO(160, 34, 37, 1),
            ),
          )
        ]
      )
    );
  }
}

