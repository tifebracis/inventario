
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';

import 'listProdutos.dart';

import '../helpers/handlerUser.dart';
import '../helpers/databaseHelper.dart';

import '../models/user.dart';
import '../models/tipoInventario.dart';

GlobalKey<_CreateInventState> globalCreateInventState = new GlobalKey<_CreateInventState>();

class HandlerCreateInvent extends StatelessWidget{
  final String userId;

  const HandlerCreateInvent({this.userId});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return (
      ListProdutos(key: globalCreateInventState,userId: userId)
    );
  }

}

class CreateInvent extends StatefulWidget {
  final String userId;

  const CreateInvent({Key key,this.userId});

  @override
  _CreateInventState createState() => _CreateInventState();
}

class _CreateInventState extends State<CreateInvent> {
  String stateDevice = '';
  String optionSelected = '';
  List<TipoInventario> tiposInventario = new List<TipoInventario>();

  final fbRef = FirebaseDatabase.instance.reference();

  bool loading = true;
  var inventExists;
  String dateNow = '';

  void initState() {
    super.initState();
    checkConect();
    existsInventarioOpen(null);
    //getTiposInveratario(); 
  }

  void goBack(){
    Navigator.pop(context);
  }

  void existsInventarioOpen(stage) async{
    await getTiposInveratario();
    new Future(() async{
      for(var t = 0; t < tiposInventario.length;t++) {
        await fbRef.child('inventario').child(tiposInventario[t].value).once().then((data){
          //print(data.value);
          if(data.value != null){
            for(int i = 0; i < data.value.length; i++){
              //print(data.value[i]['closed']);
              if(data.value[i]['closed'] == null){
                setState(() {
                  inventExists = {
                    'typeInventario': tiposInventario[t].value,
                    'dateNow': data.value[i]['dateStart']
                  };
                });
              }
            }  
          }
        });
      };
    }).then((v) async {
      print(inventExists);
      if(inventExists != null){
        if(stage != 'back'){
          final callBack = await Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => HandlerListProducts(userId: widget.userId,dateInventario: inventExists['dateNow'],typeInventario: inventExists['typeInventario'])),
          );
          setState(() {
            inventExists = null;
          });
          if(callBack == null){
            existsInventarioOpen('back');
          }else if(callBack == 'request'){
            existsInventarioOpen(null);
          }
        }else{
          goBack();
        }
      }else{
        final todayDate = (new DateTime.now().subtract(new Duration(hours: 3)));
        final day = (todayDate.day).toString();
        final month = (todayDate.month).toString();
        final year = (todayDate.year).toString();
        setState(() {
          dateNow = (day.length == 1 ? '0'+day : day)+'/'+(month.length == 1 ? '0'+month : month)+'/'+year;
        });
      }
      setState(() {
        loading = false;
      });
    });
  }

  void getTiposInveratario() async{
    await fbRef.once().then((DataSnapshot data){
      List<TipoInventario> listData = new List<TipoInventario>();
      for(int i = 0; i < data.value['tiposInventario'].length; i++){
        TipoInventario tipoInventario = TipoInventario.fromMap({'label': data.value['tiposInventario'][i]['label'],'value': data.value['tiposInventario'][i]['value']});
        listData.add(tipoInventario);
      }
      setState(() {
        optionSelected = listData.length > 0 ? listData[0].value : '';
        tiposInventario = listData;
      });
    });
  }

  void checkConect() async{
    await checkConection().then((stateDev){
      setState(() {
        stateDevice = stateDev;
      });
      if(stateDev == 'offline'){
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => HandlerListProducts(userId: '')),
        );
      }
    });
  }

  void stateLoading(){
    setState(() {
      loading = !loading;
    });
  }

  void createInventario() async{
    var list = [];
    await fbRef.child('inventario').child(optionSelected).once().then((data) async{
      if(data.value != null){
        for(int i = 0; i < data.value.length; i++){
          list.add(data.value[i]);    
        }
      }
      list.add({
        'dateStart': dateNow,
        'prods': []
      });

      fbRef.child('inventario').child(optionSelected).set(list);

      final callBack = await Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => HandlerListProducts(userId: widget.userId,dateInventario: dateNow,typeInventario: optionSelected)),
      );
      print(callBack);
      if(callBack == null){
        existsInventarioOpen('back');
      }else if(callBack == 'request'){
        existsInventarioOpen(null);
      }
    });
  }

  List<Widget> buildOptions(){
    List<Widget> options = [];
    tiposInventario.forEach((tipo){
      options.add(
        Flex(
          direction: Axis.horizontal,
          children: <Widget>[
            Radio(
              activeColor: Colors.white,
              value: optionSelected,
              groupValue: tipo.value,
              onChanged: (val) {
                setState(() { 
                  optionSelected = tipo.value; 
                });
              },
            ),
            Text(
              tipo.label,
              style: TextStyle(
                fontSize: 25,
                color: Colors.white
              ),
            )
          ],
        )
      );
    });

    return options;
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: stateDevice == 'online' ? Color.fromRGBO(160, 34, 37, 1) : Color.fromRGBO(50, 50, 50, 1),
        title: Text('Abrir inventário'),
      ),
      backgroundColor: stateDevice == 'online' ? Color.fromRGBO(160, 34, 37, 1) : Color.fromRGBO(50, 50, 50, 1),
      body: Center(
        child: Container(
          child: Builder(
            builder: (build){
              if(loading){
                return (
                  Image(
                    width: 30,
                    image: AssetImage('images/spinner.gif')
                  )
                );
              }else{
                return(
                  Flex(
                    direction: Axis.vertical,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Text(
                          dateNow,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 50
                          ),
                        ),
                      ),
                      Flex(
                        direction: Axis.horizontal,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: buildOptions(),
                      )
                    ],
                  )
                );
              }
            }
          )
        )
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Container(
        margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
        width: double.maxFinite,
        child: RaisedButton(
          color: Colors.white,
          onPressed: () {
            if(stateDevice == 'online'){
              createInventario();
            }
          },
          child: Container(
            child: Text(
              stateDevice == 'online' ? 'Abrir Inventário' : 'Offline',
              style: TextStyle(color: stateDevice == 'online' ? Color.fromRGBO(160, 34, 37, 1) : Color.fromRGBO(50, 50, 50, 1)),
            ), 
          ),
        ),
      ),
    );
  }
}