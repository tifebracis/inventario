class Product{
  int id;
  String name;
  int quant;
  String userId;
  String documentId;

  Product({
    this.id,
    this.name,
    this.quant,
    this.userId
  });

  Map<String, dynamic> toMap(){
    var map = <String, dynamic>{
      'name': name,
      'quant': quant,
      'userId': userId
    };

    return map;
  }

  Product.fromMap(Map<String, dynamic> map){
    id = map['id'];
    name = map['name'];
    quant = map['quant'];
    userId = map['userId'];
  }
}