class TipoInventario{
  String label;
  String value;
  String id;

  TipoInventario({this.label,this.value,this.id});

  TipoInventario.fromMap(Map<String, dynamic> map){
    label = map['label'];
    value = map['value'];
  }
}