
import 'package:flutter/material.dart';
import 'package:scanner/models/product.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';

import '../pages/listProdutos.dart';

import '../helpers/databaseHelper.dart';
import '../helpers/handlerUser.dart';



class CardItem extends StatefulWidget {
  final Product prod;
  final String typeInventario;
  final String dateInventario;

  CardItem({this.prod,this.typeInventario,this.dateInventario});

  @override
  _CardItemState createState() => _CardItemState();
}

class _CardItemState extends State<CardItem> {
  DatabaseHelper db = new DatabaseHelper();
  String stateDevice = '';

  bool onDragdrop = false;

  final fbRef = FirebaseDatabase.instance.reference();

  void initState() {
    super.initState();
    checkConect();
  }

  void checkConect() async{
    await checkConection().then((stateDev){
      setState(() {
          stateDevice = stateDev;
      });
    });
  }

  void deleteItem() async {
    if(stateDevice == 'offline'){
      db.deleteProduct(widget.prod).then((productDel) { 
        print(globalListProdutosState);
        globalListProdutosState.currentState.getProducts();
      });
    }else{
      fbRef.child('inventario').child(widget.typeInventario).once().then((data){
        var list = [];
        var index;
        var indexRemove;
        var listData = [];
        if(data.value != null){
          for(int i = 0; i < data.value.length;i++){
            if(data.value[i]['dateStart'] == widget.dateInventario){
              listData = data.value;
              index = i;
            }
          }
        }
        if(listData != null){
          for(int i = 0; i < listData[index]['prods'].length;i++){
            if(listData[index]['prods'][i]['name'] == widget.prod.name)
              indexRemove = i;
            else{
              list.add(listData[index]['prods'][i]);
            }
          }
        }
        fbRef.child('inventario').child(widget.typeInventario).child(index.toString()).child('prods').set(list);
        globalListProdutosState.currentState.getProducts();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var card = Card(
      child: InkWell(
        child: Container(
          padding: const EdgeInsets.all(25),
          child: Flex(
            direction: Axis.horizontal,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flex(
                direction: Axis.vertical,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: Text(
                      widget.prod.name.substring(0, widget.prod.name.length > 28 ? 28 : widget.prod.name.length)+(widget.prod.name.length > 28 ? '...' : ''),
                      style: TextStyle(
                        fontSize: 20
                      ),
                      
                    ),
                  ),
                  Container(
                    child: Text(
                      'Quantidade: '+widget.prod.quant.toString(),
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.black54
                      ),
                    ),
                  )
                ]
              ),
              Row(
                children: <Widget>[
                  /* ontainer(
                    child: Text(
                      widget.prod.quant.toString(),
                      style: TextStyle(
                        fontSize: 25
                      )
                    ),
                  ),*/
                  Container(
                    child: InkWell(
                      onTap: () {
                        deleteItem();
                      },
                      child: Container(
                        child: Icon(Icons.close,color: Colors.red)
                      )
                    )
                  )
                ]
              ),
            ],
          ),
        )
      ),
    );
    var cardBack = Card(
      child: InkWell(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.black12, Colors.red],
              stops: [0.5,0.5]
            )
          ),
          padding: const EdgeInsets.all(25),
          child: Flex(
            direction: Axis.horizontal,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flex(
                direction: Axis.vertical,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: Text(
                      '',
                      style: TextStyle(
                        fontSize: 20
                      ),
                      
                    ),
                  ),
                  Container(
                    child: Text(
                      '',
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.black54
                      ),
                    ),
                  )
                ]
              ),
              Row(
                children: <Widget>[
                  /* ontainer(
                    child: Text(
                      widget.prod.quant.toString(),
                      style: TextStyle(
                        fontSize: 25
                      )
                    ),
                  ),
                  Container(
                    child: InkWell(
                      onTap: () {
                        deleteItem();
                      },
                      child: Container(
                        child: Icon(Icons.close,color: Colors.red)
                      )
                    )
                  )*/
                ]
              ),
            ],
          ),
        )
      ),
    );
    
    
    return Container(
      child: card/*Draggable(
        axis: Axis.horizontal,
        child: onDragdrop ? cardBack: card,
        feedback: card,
        onDragStarted: (){
          setState(() {
            onDragdrop = true;
          });
        },
        onDragEnd: (e){ 
          setState(() {
            onDragdrop = false;
          });
        }
      )*/
    );
  }
}