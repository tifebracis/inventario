import 'package:cloud_firestore/cloud_firestore.dart';
import '../models/product.dart';

class DatabaseServiceFirebase{
  final CollectionReference collection = Firestore.instance.collection('inventario');

  Future insertPorduct(Product prod) async{
    return await collection.document().setData(prod.toMap());
  }
}