import 'dart:io';
import 'dart:async';

Future<String> checkConection() async{
  String stateDevice = 'offline';
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      stateDevice = 'online';
    }
  } on SocketException catch (_) {
    stateDevice = 'offline';
  }
  return stateDevice;
}