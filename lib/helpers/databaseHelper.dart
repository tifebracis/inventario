import 'dart:async';
import 'dart:io';

import '../models/product.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper{
  static DatabaseHelper databaseHelperInstance;
  static Database _database;

  String tableUserName = 'User';
  String columnUserId = 'id';
  String columnUserUserName = 'userName';
  String columnUserPassword = 'password';
  String tableProductName = 'Product';
  String columnProductId = 'id';
  String columnProductName = 'name';
  String columnProductQuant = 'quant';
  String columnProductUser = 'userId';

  DatabaseHelper.createInstance();

  factory DatabaseHelper(){
    if(databaseHelperInstance == null)
      databaseHelperInstance = DatabaseHelper.createInstance();

    return databaseHelperInstance;
  }

  Future<Database> get database async {
    if(_database == null)
      _database = await initializeDatabase();
    
    return _database;
  }

  Future<Database> initializeDatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + 'scanner.db';
    print(path);
    var scannerDatabase = await openDatabase(path, version: 1, onCreate: createDb);

    return scannerDatabase;
  }

  void createDb(Database db, int newVersion) async {
    await db.execute('CREATE TABLE $tableUserName($columnUserId INTEGER PRIMARY KEY AUTOINCREMENT,$columnUserUserName TEXT, $columnUserPassword TEXT)');
    await db.execute('CREATE TABLE $tableProductName($columnProductId INTEGER PRIMARY KEY AUTOINCREMENT,$columnProductName TEXT, $columnProductQuant INTEGER, $columnProductUser TEXT)');
  }

  Future<int> insertProduct(Product product) async {
    Database db = await database;
    var result = await db.insert(DatabaseHelper().tableProductName, product.toMap());

    return result;
  }
  Future<int> updateProduct(Product product) async {
    Database db = await database;
    var result = await db.update(DatabaseHelper().tableProductName, product.toMap());

    return result;
  }

  Future<Product> deleteProduct(Product product) async {
    Database db = await database;
    db.delete(tableProductName, where: 'id = ?', whereArgs: [product.id]);

    return product;
  }

  Future<bool> deleteAllProduct() async {
    Database db = await database;
    db.delete(tableProductName);

    return true;
  }

  Future<List<Product>> getProducts() async {
    Database db = await database;

    List<Map> maps = await db.query(tableProductName);

    List<Product> listProducts = maps.isNotEmpty ? maps.map((p) => Product.fromMap(p)).toList() : [];

    return listProducts;
  }
}